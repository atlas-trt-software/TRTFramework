#include <TRTFramework/Algorithm.h>

#include <AsgTools/StatusCode.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>

const xAOD::TrackParticleContainer* xTRT::Algorithm::trackContainer() {
  const xAOD::TrackParticleContainer* trackContainerPtr = nullptr;
  if ( evtStore()->retrieve(trackContainerPtr,"InDetTrackParticles").isFailure() ) {
    ANA_MSG_ERROR("InDetTrackParticles unavailable!");
    return nullptr;
  }
  return trackContainerPtr;
}

const xAOD::ElectronContainer* xTRT::Algorithm::electronContainer() {
  const xAOD::ElectronContainer* electronContainerPtr = nullptr;
  if ( evtStore()->retrieve(electronContainerPtr,"Electrons").isFailure() ) {
    ANA_MSG_ERROR("Electrons unavailable!");
    return nullptr;
  }
  return electronContainerPtr;
}

const xAOD::MuonContainer* xTRT::Algorithm::muonContainer() {
  const xAOD::MuonContainer* muonContainerPtr = nullptr;
  if ( evtStore()->retrieve(muonContainerPtr,"Muons").isFailure() ) {
    ANA_MSG_ERROR("Muons unavailable!");
    return nullptr;
  }
  return muonContainerPtr;
}

const xAOD::TrackParticleContainer* xTRT::Algorithm::selectedTracks() {
  return selectedContainer<xAOD::TrackParticleContainer,xAOD::TrackParticle>
    (trackContainer(),passTrackSelection,"xTRT_GoodTracks");
}

const xAOD::ElectronContainer* xTRT::Algorithm::selectedElectrons() {
  return selectedContainer<xAOD::ElectronContainer,xAOD::Electron>
    (electronContainer(),passElectronSelection,"xTRT_GoodElectrons");
}

const xAOD::MuonContainer* xTRT::Algorithm::selectedMuons() {
  return selectedContainer<xAOD::MuonContainer,xAOD::Muon>
    (muonContainer(),passMuonSelection,"xTRT_GoodMuons");
}

bool xTRT::Algorithm::triggerPassed(const std::string trigName) const {
  auto chainGroup = m_trigDecToolHandle->getChainGroup(trigName);
  auto passed = chainGroup->isPassed();
  return passed;
}

bool xTRT::Algorithm::triggersPassed(const std::vector<std::string>& trigNames) const {
  for ( const auto& name : trigNames ) {
    auto chainGroup = m_trigDecToolHandle->getChainGroup(name);
    if ( chainGroup->isPassed() ) return true;
  }
  return false;
}

bool xTRT::Algorithm::singleElectronTrigMatched(const xAOD::Electron* electron) {
  if ( not config()->useTrig() ) {
    ANA_MSG_WARNING("Asking for trigger matching without trigger enabled? Ret false");
    return false;
  }
  for ( const std::string trigstr : config()->electronTriggers() ) {
    if ( m_trigMatchingToolHandle->match(*electron,trigstr) ) {
      return true;
    }
  }
  return false;
}

bool xTRT::Algorithm::singleMuonTrigMatched(const xAOD::Muon* muon) {
  if ( not config()->useTrig() ) {
    ANA_MSG_WARNING("Asking for trigger matching without trigger enabled? Ret false");
    return false;
  }
  for ( const std::string trigstr : config()->muonTriggers() ) {
    if ( m_trigMatchingToolHandle->match(*muon,trigstr) ) {
      return true;
    }
  }
  return false;
}

float xTRT::Algorithm::eventWeight() {
  const xAOD::EventInfo* evtinfo = eventInfo();
  if ( evtinfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) {
    auto weights = evtinfo->mcEventWeights();
    if ( not weights.empty() ) return weights.at(0);
    return 1.0;
  }
  return 1.0;
}

float xTRT::Algorithm::averageMu() {
  const xAOD::EventInfo* evtinfo = eventInfo();
  if ( !(evtinfo->eventType(xAOD::EventInfo::IS_SIMULATION)) && config()->usePRW() ) {
    return m_PRWToolHandle->getCorrectedAverageInteractionsPerCrossing(*evtinfo,true);
  }
  else {
    return eventInfo()->averageInteractionsPerCrossing();
  }
}

std::size_t xTRT::Algorithm::NPV() const {
  const xAOD::VertexContainer* verts = nullptr;
  if ( evtStore()->retrieve(verts,"PrimaryVertices").isFailure() ) {
    ANA_MSG_WARNING("Cannot retrieve PrimaryVertices, returning 0");
    return 0;
  }
  return verts->size();
}

bool xTRT::Algorithm::passGRL() const {
  if ( isMC() || !config()->useGRL() ) {
    return true;
  }
  return m_GRLToolHandle->passRunLB(*eventInfo());
}

bool xTRT::Algorithm::passTrackSelection(const xAOD::TrackParticle* track, const xTRT::Config* conf) {
  if ( xTRT::nTRT(track) < conf->track_nTRT() ) return false;
  if ( xTRT::nTRT_PrecTube(track) < conf->track_nTRTprec() ) return false;
  if ( xTRT::nSilicon(track) < conf->track_nSi() ) return false;
  if ( track->pt()*toGeV < conf->track_pT() ) return false;
  if ( std::abs(track->eta()) > conf->track_eta() ) return false;
  if ( track->p4().P()*toGeV < conf->track_p() ) return false;
  return true;
}

bool xTRT::Algorithm::passElectronSelection(const xAOD::Electron* electron, const xTRT::Config* conf) {
  auto trk = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);

  if ( conf->elec_truthMatched() ) {
    if ( not truthMatched(electron) ) return false;
    bool fromZ   = isFromZ(electron);
    if ( conf->elec_fromZ() && (not fromZ) ) return false;
    bool fromJ   = isFromJPsi(electron);
    if ( conf->elec_fromJPsi() && (not fromJ) ) return false;
    bool fromZoJ = (fromZ || fromJ);
    if ( conf->elec_fromZorJPsi() && (not fromZoJ) ) return false;
  }

  if ( conf->elec_UTC() ) {
    if ( trk == nullptr ) {
      XTRT_WARNING("No track from electron! failing selection");
      return false;
    }
    else {
      if ( not passTrackSelection(trk,conf) ) return false;
    }
  }

  if ( conf->elec_relpT() > 0 ) {
    if ( trk == nullptr ) {
      XTRT_WARNING("No track particle from electron! relative pT cut failing");
      return false;
    }
    else {
      if ( trk->pt() < (conf->elec_relpT() * electron->pt()) ) return false;
    }
  }

  if ( electron->pt()*toGeV < conf->elec_pT() ) return false;
  if ( electron->p4().P()*toGeV < conf->elec_p() ) return false;
  if ( std::abs(electron->eta()) > conf->elec_eta() ) return false;

  return true;
}

bool xTRT::Algorithm::passMuonSelection(const xAOD::Muon* muon, const xTRT::Config* conf) {
  auto idtl = muon->inDetTrackParticleLink();
  if ( not idtl.isValid() ) return false;
  auto trk = *idtl;

  if ( conf->muon_truthMatched() ) {
    if ( not truthMatched(muon) ) return false;
    bool fromZ   = isFromZ(muon);
    if ( conf->muon_fromZ() && (not fromZ) ) return false;
    bool fromJ   = isFromJPsi(muon);
    if ( conf->muon_fromJPsi() && (not fromJ) ) return false;
    bool fromZoJ = (fromZ || fromJ);
    if ( conf->muon_fromZorJPsi() && (not fromZoJ) ) return false;
  }

  if ( conf->muon_UTC() ) {
    if ( trk == nullptr ) {
      XTRT_WARNING("No valid muon->inDetTrackParticleLink()! auto failing track selection");
      return false;
    }
    else {
      if ( not passTrackSelection(trk,conf) ) return false;
    }
  }

  if ( conf->muon_relpT() > 0 ) {
    if ( trk == nullptr ) {
      XTRT_WARNING("No valid muon->inDetTrackParticleLink()! auto failing rel pT selection");
      return false;
    }
    else {
      if ( trk->pt() < (conf->muon_relpT() * muon->pt()) ) return false;
    }
  }

  if ( muon->pt()*toGeV < conf->muon_pT() ) return false;
  if ( muon->p4().P()*toGeV < conf->muon_p() ) return false;
  if ( std::abs(muon->eta()) > conf->muon_eta() ) return false;

  return true;
}
